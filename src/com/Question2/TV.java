package com.Question2;

public class TV implements SmartTVremote {
	  @Override
	  public void power() {
	    System.out.println("Turn the TV on/off.");
	  }

	  @Override
	  public void volume() {
	    System.out.println("Adjust the TV volume.");
	  }

	  @Override
	  public void channel() {
	    System.out.println("Change the TV channel.");
	  }

	  @Override
	  public void mute() {
	    System.out.println("Mute the TV sound.");
	  }
	}







