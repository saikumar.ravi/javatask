package com.Question2;

public class Helloworld {

	public static void main(String[] args) {
		 String sentence = "Hello,World";
		    int firstIndex = sentence.indexOf('o');
		    int lastIndex = sentence.lastIndexOf('o');
		    int commaIndex = sentence.indexOf(',');
		    System.out.println("First occurrence of 'o': " + firstIndex);
		    System.out.println("Last occurrence of 'o': " + lastIndex);
		    System.out.println("First occurrence of ',': " + commaIndex);
	}

}
