package com.Question2;

public class SmartTelephone extends Telephone {
	  @Override
	  void pickup() {
	    System.out.println("Pick up the phone.");
	  }

	  @Override
	  void lift() {
	    System.out.println("Lift the phone.");
	  }

	  @Override
	  void disconnect() {
	    System.out.println("Disconnect the phone call.");
	  }
	}


