package com.FindArea;

import java.util.Scanner;

public class FindShapes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		 Scanner sc = new Scanner(System.in);
		    
		    System.out.println("Enter the length and breadth of the rectangle:");
		    int length = sc.nextInt();
		    int breadth = sc.nextInt();
		    int rectangleArea = length * breadth;
		    System.out.println("Area of rectangle: " + rectangleArea);
		    
		    System.out.println("Enter the side of the square:");
		    int side = sc.nextInt();
		    int squareArea = side * side;
		    System.out.println("Area of square: " + squareArea);
		    
		    System.out.println("Enter the radius of the circle:");
		    int radius = sc.nextInt();
		    double circleArea = Math.PI * radius * radius;
		    System.out.println("Area of circle: " + circleArea);
		  }
		}
	


