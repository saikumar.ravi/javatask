package com.FindArea;

import java.util.Scanner;

public class Area extends Shape {
	  void RectangleArea(double length, double breadth) {
		    System.out.println("Area of Rectangle: " + (length * breadth));
		  }

		  void SquareArea(double side) {
		    System.out.println("Area of Square: " + (side * side));
		  }

		  void CircleArea(double radius) {
		    System.out.println("Area of Circle: " + (3.14 * radius * radius));
		  }
		

		
public static void main(String[] args) {
	Area obj = new Area();
    obj.RectangleArea(10, 20);
    obj.SquareArea(15);
    obj.CircleArea(5);
	
}
}
