package com.Collections;

import java.util.ArrayList;

public class ArrayListProgram {

	public static void main(String[] args) {
		ArrayList<String> products = new ArrayList<>();
	    products.add("Pepsi");
	    products.add("Sprite");
	    products.add("Fanta");
	    products.add("ThumbsUp");
	    products.add("7UP");
	    
	    System.out.println("Size of list: " + products.size());
	    System.out.println("Contents of list: " + products);
	    
	    products.remove(1);
	    System.out.println("After removal, size of list: " + products.size());
	    System.out.println("Contents of list: " + products);
	    
	    boolean containsCoke = products.contains("Coke");
	    System.out.println("List contains Coke: " + containsCoke);
	  }
	}


