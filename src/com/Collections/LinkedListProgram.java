package com.Collections;

import java.util.LinkedList;

public class LinkedListProgram {

	public static void main(String[] args) {
		LinkedList<String> products = new LinkedList<>();
	    products.add("Pepsi");
	    products.add("Sprite");
	    products.add("Fanta");
	    products.add("ThumbsUp");
	    products.add("Coke");
	    
	    System.out.println("Size of list: " + products.size());
	    System.out.println("Contents of list: " + products);
	    
	    products.remove("Sprite");
	    System.out.println("After removal, size of list: " + products.size());
	    System.out.println("Contents of list: " + products);
	    
	    boolean containsCoke = products.contains("Coke");
	    System.out.println("List contains Coke: " + containsCoke);
	}

}
